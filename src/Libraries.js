const fs = require('fs');

const del = require('del');
const gulp = require('gulp');
const gulpFilter = require('gulp-filter');
const gulpRename = require('gulp-rename');
const npmfiles = require('npmfiles');
const path = require('path');
const Config = require('./Config');

class Libraries {
	constructor(config) {
		this.libsConfig = new Config(config).libs;
		this.bower = this.bower.bind(this);
		this.cleanLibs = this.cleanLibs.bind(this);
		this.customizeBootstrap = this.customizeBootstrap.bind(this);
		this.fetchBower = this.fetchBower.bind(this);
		this.libs = this.libs.bind(this);
		this.npm = this.npm.bind(this);
		this.processBower = this.processBower.bind(this);
		this.proprietaryLibs = this.proprietaryLibs.bind(this);
	}

	cleanLibs() {
		return del([this.libsConfig.dst], { force: true });
	}

	customizeBootstrap() {
		const gulpLess = require('gulp-less');
		return gulp.src(`${this.libsConfig.less}/vendors/bootstrap.less`)
			.pipe(gulpLess())
			.pipe(gulp.dest(`${this.libsConfig.dst}/bootstrap/dist/css`));
	}

	fetchBower() {
		const gulpBower = require('gulp-bower');
		return gulpBower().pipe(gulp.dest(this.libsConfig.bowerComponents));
	}

	processBower() {
		const gulpSass = require('gulp-sass');
		const mainBowerFiles = require('main-bower-files');
		const bowerFiles = mainBowerFiles();
		const notScssFilter = gulpFilter(['**/*', '!**/*.scss'], { restore: true });
		const scssFilter = gulpFilter(['**/*.scss']);
		const stream = gulp.src(bowerFiles, { base: this.libsConfig.bowerComponents })
			.pipe(notScssFilter)
			.pipe(gulpRename((filePath) => {
				filePath.basename = filePath.basename.replace('.min', '');
			}))
			.pipe(gulp.dest(this.libsConfig.dst));
		notScssFilter.restore.pipe(scssFilter).pipe(gulpSass()).pipe(gulp.dest(this.libsConfig.dst));
		return stream;
	}

	bower(done) {
		return gulp.series(this.fetchBower, this.processBower)(done);
	}

	npm()	{
		return gulp.src(
			npmfiles({ nodeModulesPath: path.resolve(this.libsConfig.nodeModules) }),
			{ base: this.libsConfig.nodeModules },
		).pipe(gulp.dest(this.libsConfig.dst));
	}

	proprietaryLibs() {
		const proprietaryFiles = `${this.libsConfig.proprietary}/**`;
		return gulp.src(proprietaryFiles)
			// XXX: add filters for ThreeGraph
			.pipe(gulpFilter((file) => {
				const newPath = file.path.replace(/.([^.]+)$/g, '.min.$1');
				return !fs.existsSync(newPath);
			}))
			.pipe(gulpRename((filePath) => {
				filePath.basename = filePath.basename.replace('.min', '');
			}))
			.pipe(gulp.dest(this.libsConfig.dst));
	}

	libs(done) {
		const tasksQueue = [this.cleanLibs, this.npm];
		if (fs.existsSync('./bower.json')) {
			tasksQueue.push(this.bower);
		}
		if (this.libsConfig.proprietary) {
			tasksQueue.push(this.proprietaryLibs);
		}
		return gulp.series(...tasksQueue)(done);
	}
}

module.exports = Libraries;
