const gulp = require('gulp');
const gitmodified = require('gulp-gitmodified');
const PluginError = require('plugin-error');
const gulpJsonSort = require('gulp-json-sort').default;
const gulpEol = require('gulp-eol');
const gulpDiff = require('gulp-diff');
const gulpPlumber = require('gulp-plumber');
const glob = require('glob');
const path = require('path');
const prettyjson = require('prettyjson');
const _ = require('lodash');

const Config = require('./Config');

class Validator {
	constructor(config) {
		this.config = new Config(config);
		this.requiredExtension = this.config.hasTypescript() ? '.ts' : this.config.getJSExtension();
		this.newFilesExtension = this.newFilesExtension.bind(this);
		this.sortLocalizations = this.sortLocalizations.bind(this);
		this.checkLocalizations = this.checkLocalizations.bind(this);
	}

	calcForbiddenPaths() {
		const forbiddenPaths = [`${this.config.getAppRoot()}**/*.js`];
		if (this.config.hasTypescript()) {
			forbiddenPaths.push(`${this.config.getAppRoot()}**/*.js6`);
		}
		return forbiddenPaths;
	}

	newFilesExtension() {
		return gulp.src(this.calcForbiddenPaths())
			.pipe(gitmodified({ modes: 'added', gitCwd: this.config.getGitRoot(), stagedOnly: true }))
			.on('data', (file) => {
				throw new PluginError('Validator', `Необходимо использовать ${this.requiredExtension} для новых файлов: ${file.path}`);
			});
	}

	sortLocalizations() {
		let errors = 0;
		return gulp.src([`${this.config.paths.localization.base}/**/*.json`])
			.pipe(gulpJsonSort({
				cmp: (left, right) => {
					if (typeof left.value === 'string' && typeof right.value === 'object') {
						return -1;
					}
					if (typeof right.value === 'string' && typeof left.value === 'object') {
						return 1;
					}
					const leftUpperCased = left.key.toUpperCase();
					const rightUpperCased = right.key.toUpperCase();
					// we use CAPS_CASE for System namespaces
					if (left.key === leftUpperCased && right.key !== rightUpperCased) {
						return 1;
					}
					if (left.key !== leftUpperCased && right.key === rightUpperCased) {
						return -1;
					}
					if (leftUpperCased > rightUpperCased) {
						return 1;
					}
					if (leftUpperCased < rightUpperCased) {
						return -1;
					}
					return left.key > right.key ? 1 : -1;
				},
				space: 2,
			}))
			.pipe(gulpEol())
			.pipe(gulpPlumber())
			.pipe(gulpDiff(this.config.paths.localization.base))
			.pipe(gulp.dest(this.config.paths.localization.base))
			.pipe(gulpDiff.reporter({quiet: true, fail: true}).on('error', () => {
				errors++;
			}))
			.on('finish', () => {
				if (errors) {
					throw new PluginError({
						plugin: 'sortLocalizations',
						message: 'Localizations were sorted, please stage changes',
					});
				}
			});
	}

	checkLocalizations() {
		const result = {
			missingKeys: [],
			redundantKeys: [],
			missingFiles: [],
		};
		return new Promise((resolve, reject) => {
			const pathRu = `${path.resolve(this.config.paths.localization.rus)}/`;
			const pathEn = `${path.resolve(this.config.paths.localization.eng)}/`;
			const ignore = this.config.paths.localization.ignore;
			glob(`${pathRu}**/*.json`, { ignore: ignore ? ignore.map(fileName => pathRu + fileName) : null }, (e, files) => {
				files.forEach((file) => {
					const fileName = path.basename(file);
					const base = require(pathRu + fileName);
					const second = require(pathEn + fileName);
					if (second) {
						this.mergeKeys(result, this.compareByKey(base, second, `${fileName}#`));
					} else {
						result.missingFiles.push(pathEn + fileName);
					}
				});
				if (result.missingKeys.length || result.redundantKeys.length || result.missingFiles.length) {
					console.log(prettyjson.render(result));
					reject(new PluginError({
						plugin: 'checkLocalizations',
						message: 'Localization problem',
					}))
				} else {
					resolve();
				}
			});
		});
	}

	compareByKey(o1, o2, keyPath) {
		const result = {
			missingKeys: [],
			redundantKeys: [],
		};
		const keys1 = Object.keys(o1);
		const keys2 = Object.keys(o2);
		let absentKeys = _.difference(keys1, keys2);
		let redundantKeys = _.difference(keys2, keys1);
		const PLURAL_SUFFIX_ENG = '_plural';
		const pluralEngKeys = absentKeys.reduce((engKeys, key) => {
			if (key.endsWith('_0')) {
				const word = key.substr(0, key.length - 2);
				engKeys.push(word, `${word}${PLURAL_SUFFIX_ENG}`);
			}
			return engKeys;
		}, []);
		const pluralRusKeys = redundantKeys.reduce((rusKeys, key) => {
			if (key.endsWith(PLURAL_SUFFIX_ENG)) {
				const word = key.substr(0, key.length - PLURAL_SUFFIX_ENG.length);
				rusKeys.push(`${word}_0`, `${word}_1`, `${word}_2`);
			}
			return rusKeys;
		}, []);
		const missingPluralRusKeys = _.difference(pluralRusKeys, absentKeys);
		absentKeys = _.difference(absentKeys, pluralRusKeys)
			.filter(key => !(key.endsWith('_0') || key.endsWith('_1') || key.endsWith('_2')));
		absentKeys.push(...missingPluralRusKeys);

		const missingPluralEngKeys = _.difference(pluralEngKeys, redundantKeys)
		redundantKeys = _.difference(redundantKeys, pluralEngKeys);
		absentKeys.push(...missingPluralEngKeys);

		absentKeys.forEach((key) => {
			result.missingKeys.push(keyPath + key);
		});

		redundantKeys.forEach((key) => {
			result.redundantKeys.push(keyPath + key);
		});
		keys1.forEach((key) => {
			const val1 = o1[key];
			if (val1 instanceof Object && absentKeys.indexOf(key) === -1) {
				this.mergeKeys(result, this.compareByKey(val1, o2[key], `${keyPath + key}.`));
			}
		});
		return result;
	}

	mergeKeys(keys1, keys2) {
		keys1.missingKeys = keys1.missingKeys.concat(keys2.missingKeys);
		keys1.redundantKeys = keys1.redundantKeys.concat(keys2.redundantKeys);
	}
}

module.exports = Validator;

