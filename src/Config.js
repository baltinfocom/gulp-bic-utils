const { argv } = require('yargs');

const { readJson } = require('./Utils');

const defaultConfig = require('./defaultConfig.json');

const config = readJson('./config.json');
const packageJson = readJson('./package.json');

class Config {
	constructor(customConfig = config) {
		this.paths = Object.assign(defaultConfig.paths, customConfig.paths);
		this.languages = Object.assign(defaultConfig.languages, customConfig.languages);
		this.libs = Object.assign(defaultConfig.libs, customConfig.libs);
		this.transpiler = Object.assign({
			'tsConfig': './tsconfig.json',
		}, customConfig.transpiler);
		this.linter = Object.assign(defaultConfig.linter, customConfig.linter);
		this.release = Object.assign(defaultConfig.release, customConfig.release);
		this.paths.pomRoot = this.paths.pomRoot || this.paths.gitRoot;
		this.babel = customConfig.babel;
		this.durandal = customConfig.durandal;
		this._isRelease = customConfig.isRelease || argv.release;
		if (this.languages.javascript) {
			this.languages.javascript = Array.isArray(this.languages.javascript) ? this.languages.javascript : ['.js'];
		}
	}

	hasJava() {
		return this.languages.java;
	}

	hasJavascript() {
		return !!this.languages.javascript;
	}

	hasTypescript() {
		return this.languages.typescript;
	}

	hasLegacyJS() {
		return this.getJSExtension() === '.js6';
	}

	getBabelModules() {
		return this.babel.modules;
	}

	getBabelTarget() {
		return this.isRelease() ? this.babel.targets.release : this.babel.targets.debug;
	}

	getTranspilerDst() {
		return this.isRelease() ? this.transpiler.dst.release : this.getTranspilerDstDebug(packageJson.version);
	}

	getTranspilerDstDebug(version) {
		// eslint-disable-next-line no-template-curly-in-string
		return this.transpiler.dst.debug.replace('${version}', version);
	}

	getGitRoot() {
		return this.paths.gitRoot;
	}

	getAppRoot() {
		return this.paths.appRoot;
	}

	getJSPaths() {
		if (!this.languages.javascript) {
			return null;
		}
		return this.languages.javascript.map(ext => `${this.getAppRoot()}**/*${ext}`);
	}

	getTSPaths() {
		if (this.languages.typescript) {
			return [`${this.getAppRoot()}**/*.ts`];
		}
	}

	getJSExtension() {
		if (!this.languages.javascript) {
			return null;
		}
		return this.languages.javascript[0];
	}

	isRelease() {
		return this._isRelease;
	}
}

module.exports = Config;
