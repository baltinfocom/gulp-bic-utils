const gulp = require('gulp');
const mocha = require('gulp-mocha');

module.exports = testsPath => () => gulp.src([`${testsPath}**/*.js`], { read: false })
	.pipe(mocha({ reporter: 'nyan' }));
