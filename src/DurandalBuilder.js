const fs = require('fs');

const gulp = require('gulp');
const del = require('del');
const htmlReplace = require('gulp-html-replace');
const dateFormat = require('dateformat');
const gulpTerser = require('gulp-terser');
const PluginError = require('plugin-error');

const Config = require('./Config');
const Transpiler = require('./Transpiler');

class DurandalHelper {
	constructor(config, opts) {
		this.config = config;
		this.setOpts(opts);

		this.build = this.build.bind(this);
		this.durandalBuild = this.durandalBuild.bind(this);
		this.mainReplace = this.mainReplace.bind(this);
	}

	setOpts(opts) {
		this.opts = Object.assign({
			toExclude: [],
			extraModules: [],
			main: 'index.js',
			requireConfig: 'require-config.js',
		}, opts);
		this.opts.output = opts.output || opts.main.replace('.js', `.${dateFormat(new Date(), 'dd.mm.yy')}.min.js`);
		this.opts.requireConfig = `${this.config.getAppRoot()}${opts.requireConfig}`;
		this.opts.htmlRoot = `${this.config.durandal.paths.webappRoot}${this.opts.htmlRoot}`;
	}

	mainReplace() {
		return gulp.src(this.opts.htmlRoot)
			.pipe(htmlReplace({
				main: {
					src: `build/${this.opts.output}`,
					tpl: '<!-- build:main -->\n<script data-main="%s" src="vendors/requirejs/require.js"></script>\n<!-- endbuild -->',
				},
			}))
			.pipe(gulp.dest(this.config.durandal.paths.webappRoot));
	}

	getRequireJSConfig(requireConfigPath) {
		return eval(`(${fs.readFileSync(requireConfigPath, { encoding: 'utf-8' }).match('config = ({[^;]*});')[1]})`);
	}

	durandalBuild(done) {
		const durandal = require('gulp-durandal');
		durandal({
			baseDir: this.config.getTranspilerDst(),
			main: this.opts.main,
			output: this.opts.output,
			extraModules: this.opts.extraModules,
			almond: false,
			require: true,
			minify: false,
			verbose: false,
			moduleFilter: moduleName => this.opts.toExclude.every(path => !moduleName.includes(path)),
			rjsConfigAdapter: (rjsConfig) => {
				rjsConfig.generateSourceMaps = false;
				// исключаем библиотеки
				const requireConfig = this.getRequireJSConfig(this.opts.requireConfig);
				Object.assign(rjsConfig, requireConfig);
				// убираем модули, добавленные в exclude в moduleFilter, чтобы r.js не пытался разрешить для них зависимости
				rjsConfig.exclude = [];
				Object.keys(requireConfig.paths).forEach((pathName) => {
					const path = requireConfig.paths[pathName];
					if (path.includes('vendors') && !path.includes('durandal')) {
						rjsConfig.exclude.push(pathName);
					}
				});
				return rjsConfig;
			},
		})
			.on('error', (err) => {
				done(new PluginError('durandal', err));
			})
			// strip debug removes all console.* calls from result file
			// pipe(plugins.stripDebug() ).
			.pipe(gulpTerser({
				mangle: true,
				compress: false,
				keep_fnames: true,
			}))
			.pipe(gulp.dest(this.config.durandal.paths.dst))
			.on('end', done);
	}

	build(done) {
		return gulp.series(this.durandalBuild, this.mainReplace)(done);
	}
}

class DurandalBuilder {
	constructor(config) {
		this.config = new Config(config);

		this.tasks = this.config.durandal.builds.map(opts => new DurandalHelper(this.config, opts).build);
		this.transpiler = new Transpiler(Object.assign({ release: true }, config));

		this.cleanJSBuild = this.cleanJSBuild.bind(this);
		this.release = this.release.bind(this);
	}

	cleanJSBuild() {
		return del([this.config.durandal.paths.dst], { force: true });
	}

	release(done) {
		return gulp.series(this.cleanJSBuild, this.transpiler.transpile, gulp.parallel(...this.tasks))(done);
	}
}

module.exports = DurandalBuilder;
