const gulp = require('gulp');
const maven = require('maven');
const inquirer = require('inquirer');
const semver = require('semver');
const { exec } = require('child_process');
const { argv } = require('yargs');

const { readJson, writeJson } = require('./Utils');
const Config = require('./Config');

const promptReleaseVersion = () => inquirer.prompt({
	type: 'input',
	name: 'version',
	message: 'Release version',
	validate: (input) => {
		if (semver.valid(input)) {
			return true;
		}
		return 'Invalid semantic version';
	},
}).then((result) => {
	argv.releaseVersion = result.version;
});

const	updatePackageJsonVersion = (done) => {
	const packageJsonPath = './package.json';
	const packageJson = readJson(packageJsonPath);
	packageJson.version = argv.releaseVersion;
	writeJson(packageJsonPath, packageJson);
	done();
};

const changelogRelease = () => new Promise((resolve) => {
	exec(['yarn', 'changelog-manager', 'release', '-t', '--ci', `--release-version=${argv.releaseVersion}`].join(' '), (error, result) => {
		if (error) {
			resolve(error);
		} else {
			console.log(result);
			resolve();
		}
	});
});

class Releaser {
	constructor(config) {
		this.config = new Config(config);
		this.release = this.release.bind(this);
		this.setVersion = this.setVersion.bind(this);
		this.updateMavenVersion = this.updateMavenVersion.bind(this);
		this.updateLegacyTSConfig = this.updateLegacyTSConfig.bind(this);
	}

	release(done) {
		const tasks = [updatePackageJsonVersion];
		if (this.config.hasJava()) {
			tasks.push(this.updateMavenVersion);
		}
		if (this.config.release.changelog) {
			tasks.push(changelogRelease);
		}
		if (this.config.hasTypescript() && this.config.hasLegacyJS()) {
			tasks.push(this.updateLegacyTSConfig);
		}
		return gulp.series(
			promptReleaseVersion,
			gulp.parallel(tasks),
		)(done);
	}

	setVersion(done) {
		const tasks = [updatePackageJsonVersion];
		if (this.config.hasJava()) {
			tasks.push(this.updateMavenVersion);
		}
		if (this.config.hasTypescript() && this.config.hasLegacyJS()) {
			tasks.push(this.updateLegacyTSConfig);
		}
		return gulp.series(promptReleaseVersion, gulp.parallel(tasks))(done);
	}

	updateMavenVersion() {
		const mvn = maven.create({
			cwd: this.config.paths.pomRoot,
		});
		return mvn.execute('versions:set', {
			generateBackupPoms: false,
			newVersion: argv.releaseVersion,
		});
	}

	// should be used only to work with js6 files
	updateLegacyTSConfig(done) {
		const tsConfigPath = this.config.linter.tsConfig;
		const tsConfig = readJson(tsConfigPath);
		const relativeLevel = this.config.getAppRoot().split('/').length;
		let relativePath = '';
		for (let i = 0; i < relativeLevel - 1; i++) {
			relativePath += '../';
		}

		const paths = [
			'*',
			`${relativePath}${this.config.transpiler.dst.release}*`,
			`${relativePath}${this.config.getTranspilerDstDebug(argv.releaseVersion)}*`,
		];
		tsConfig.compilerOptions.paths['*'] = paths;
		writeJson(tsConfigPath, tsConfig);
		done();
	}
}

module.exports = Releaser;
