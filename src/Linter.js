const gulp = require('gulp');
const eslint = require('gulp-eslint');
const gitmodified = require('gulp-gitmodified');
const { argv } = require('yargs');
const gulpIf = require('gulp-if');
const streamToArray = require('stream-to-array');
const PluginError = require('plugin-error');

const Config = require('./Config');

const lintES = (paths, gitRoot) => () => {
	return gulp.src(paths)
		.pipe(gulpIf(!argv.all, gitmodified({ modes: ['added', 'modified'], gitCwd: gitRoot, stagedOnly: true })))
		.pipe(eslint({ fix: argv.fix }))
		.pipe(eslint.format())
		.pipe(eslint.failAfterError());
};

const lintTS = (paths, gitRoot, tsProgram) => () => {
	const gulpTSLint = require('gulp-tslint');
	return gulp.src(paths)
		.pipe(gulpIf(!argv.all, gitmodified({ modes: ['added', 'modified'], gitCwd: gitRoot, stagedOnly: true })))
		.pipe(gulpTSLint({ program: tsProgram, formatter: 'stylish', fix: argv.fix }))
		.pipe(gulpTSLint.report({
			allowWarnings: true,
			summarizeFailureOutput: true,
		}))
};

class Linter {
	constructor(config) {
		this.config = new Config(config);
		if (this.config.hasTypescript()) {
			const tslint = require('tslint');
			this.tsProgram = tslint.Linter.createProgram(this.config.linter.tsConfig);
		}
		this.lintES = this.lintES.bind(this);
		this.lintTS = this.lintTS.bind(this);
		this.lint = this.lint.bind(this);
		this.checkstyle = this.checkstyle.bind(this);
	}

	checkstyle() {
		const checkstyleBic = require('checkstyle-bic');
		const options = argv.branch ? {
			modes: ['added', 'modified'],
			targetBranch: argv.branch,
		}: {
			modes: ['added', 'modified'],
			stagedOnly: true
		};
		options.gitCwd = this.config.getGitRoot();
		const stream = gulp.src([`${this.config.getGitRoot()}**/*.java`])
			.pipe(gulpIf(!argv.all, gitmodified(options)));
		return streamToArray(stream)
			.then((files) => {
				if (files && files.length) {
					return checkstyleBic(files.map(file => file.path), this.config.getGitRoot());
				}
			})
			.catch((error) => {
				throw new PluginError('checkstyle', error);
			});
	}

	lintES(done) {
		return lintES(this.config.getJSPaths(), this.config.getGitRoot())(done);
	}

	lintTS(done) {
		let tsPaths = this.config.getTSPaths();
		let gitRoot = this.config.getGitRoot();
		return gulp.series(lintTS(tsPaths, gitRoot, this.tsProgram), lintES(tsPaths, gitRoot))(done);
	}

	// supports --all - to run for all files
	lint(done) {
		const tasks = [];
		if (this.config.hasJavascript()) {
			tasks.push(this.lintES);
		}
		if (this.config.hasTypescript()) {
			tasks.push(this.lintTS);
		}
		if (this.config.hasJava()) {
			tasks.push(this.checkstyle);
		}
		return gulp.series(...tasks)(done);
	}
}

module.exports = Linter;
