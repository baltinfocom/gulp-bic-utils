const fs = require('fs');

module.exports = {
	readJson: filePath => JSON.parse(fs.readFileSync(filePath, 'utf8')),
	writeJson: (filePath, json) => (fs.writeFileSync(filePath, `${JSON.stringify(json, null, '  ')}\n`))
};
