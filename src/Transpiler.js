const gulp = require('gulp');
const babel = require('gulp-babel');
const sourcemaps = require('gulp-sourcemaps');
const newer = require('gulp-newer');
const rename = require('gulp-rename');
const notify = require('gulp-notify');
const gulpIf = require('gulp-if');
const gulpMarkdown = require('gulp-markdown');
const del = require('del');
const path = require('path');
const { argv } = require('yargs');

const Config = require('./Config');

class Transpiler {
	constructor(config) {
		this.config = new Config(config);
		if (this.config.hasTypescript()) {
			const ts = require('gulp-typescript');
			this.tsProject = ts.createProject(this.config.transpiler.tsConfig);
		}

		this.resourcesPaths = [
			`${this.config.getAppRoot()}**/*.html`,
			`${this.config.getAppRoot()}**/*.json`,
		];

		this.transpile = this.transpile.bind(this);
		this.cleanJSDst = this.cleanJSDst.bind(this);
		this.distBabel = this.distBabel.bind(this);
		this.distBabelAmd = this.distBabelAmd.bind(this);
		this.distBabelEs6 = this.distBabelEs6.bind(this);
		this.distResources = this.distResources.bind(this);
		this.distMarkdown = this.distMarkdown.bind(this);
		this.distBabelTS = this.distBabelTS.bind(this);
		this.syncDeletedFiles = this.syncDeletedFiles.bind(this);
		this.watch = this.watch.bind(this);
	}

	needsSourceMaps() {
		return !this.config.isRelease() || argv.sourceMaps;
	}

	distBabelHelper(inputStream, successMessage, modules = this.config.getBabelModules()) {
		let stream = inputStream;
		if (this.needsSourceMaps() && successMessage !== 'ts') {
			stream = stream.pipe(sourcemaps.init());
		}
		stream = stream.pipe(babel({
			presets: [['@babel/preset-env', { targets: this.config.getBabelTarget(), modules }]],
		})).on('error', notify.onError('Error: <%= error.message %>'));
		if (this.needsSourceMaps()) {
			stream = stream.pipe(sourcemaps.write('.')).pipe(notify({ message: successMessage, onLast: true }));
		}
		return stream.pipe(gulp.dest(this.config.getTranspilerDst()));
	}

	distBabelEs6() {
		const stream = gulp.src(`${this.config.getAppRoot()}**/*${this.config.getJSExtension()}`)
			.pipe(newer({ dest: this.config.getTranspilerDst(), ext: '.js' }))
			.pipe(rename({ extname: '.js' }));
		return this.distBabelHelper(stream, this.config.getJSExtension());
	}

	distBabelTS() {
		const stream = this.tsProject.src()
			.pipe(newer({ dest: this.config.getTranspilerDst(), ext: '.js' }))
			.pipe(gulpIf(this.needsSourceMaps(), sourcemaps.init()))
			.pipe(this.tsProject());
		return this.distBabelHelper(stream, 'ts', false);
	}

	distBabelAmd() {
		const stream = gulp.src(`${this.config.getAppRoot()}**/*.js`)
			.pipe(newer(this.config.getTranspilerDst()));
		return this.distBabelHelper(stream, 'amd', false);
	}

	cleanJSDst() {
		return del([this.config.getTranspilerDst()], { force: true });
	}

	distBabel(done) {
		let distBabelJS = this.distBabelEs6;
		if (this.config.hasLegacyJS()) {
			distBabelJS = gulp.parallel(this.distBabelAmd, this.distBabelEs6);
		}
		if (this.config.hasTypescript()) {
			return gulp.series(distBabelJS, this.distBabelTS)(done);
		}
		return distBabelJS(done);
	}

	distResources() {
		return gulp.src(this.resourcesPaths)
			.pipe(newer(this.config.getTranspilerDst()))
			.pipe(gulpIf(!this.config.isRelease(), notify({ message: 'resources', onLast: true })))
			.pipe(gulp.dest(this.config.getTranspilerDst()));
	}

	distMarkdown() {
		if (!this.config.transpiler.markdown) {
			return Promise.resolve();
		}
		return gulp.src(this.config.transpiler.markdown)
			.pipe(gulpMarkdown())
			.pipe(gulp.dest(this.config.getTranspilerDst()));
	}

	transpile(done) {
		return gulp.series(this.cleanJSDst, gulp.parallel(this.distBabel, this.distResources, this.distMarkdown))(done);
	}

	syncDeletedFiles(filePath) {
		let relativePath = path.relative(path.resolve(this.config.getAppRoot()), filePath);
		const ext = relativePath.split('.').pop();
		if (ext === 'js6' || ext === 'ts') {
			relativePath = relativePath.replace(new RegExp(`${ext}$`), 'js');
		}
		del.sync([relativePath], { cwd: this.config.getTranspilerDst() });
	}

	watch() {
		const scriptsWatcher = gulp.watch(
			[`${this.config.getAppRoot()}**/*.js6`, `${this.config.getAppRoot()}**/*.js`, `${this.config.getAppRoot()}**/*.ts`],
			this.distBabel,
		);
		const resourcesWatcher = gulp.watch(this.resourcesPaths, this.distResources);
		scriptsWatcher.on('unlink', this.syncDeletedFiles);
		resourcesWatcher.on('unlink', this.syncDeletedFiles);
	}
}

module.exports = Transpiler;
